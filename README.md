### Playable commands
Sauce: https://docs.docker.com/language/nodejs/

##### Prepare node thingies
Init:
```bash
mkdir -p app && cd app
npm init -y
npm install ronin-server ronin-mocks ronin-database nodemon
```

Add `debug` script to `package.json`:
```json
{
...
  "scripts": {
    "debug": "nodemon --inspect=0.0.0.0:9229 server.js",
    "start": "node server.js"
  },
...
}

```

Verify it:
```bash
node server.js
```

##### As always
```bash
docker-compose build
docker-compose up -d && docker-compose logs -f
```

##### Post them in
```bash
curl --silent --request POST \
  --url http://localhost:8000/test \
  --header 'content-type: application/json' \
  --data '{"msg": "testing"}' | jq
```
```bash
curl --silent --request POST \
  --url http://localhost:8000/notes \
  --header 'content-type: application/json' \
  --data '{
"name": "this is a note",
"text": "this is a note that I wanted to take while I was working on writing a blog post.",
"owner": "peter"
}'
```
##### Get them out
```bash
curl -sXGET localhost:8000/test | jq
```
```bash
curl -sXGET localhost:8000/notes | jq
```

##### Connect to a debugger
open a new tab: type in `about:inspect` -> `chrome://inspect/#devices`
```bash
curl -sXGET localhost:8000/foo | jq
```

##### Add test
```bash
cd app
npm install --save-dev mocha
```
Add/Update `test` script to `package.json`:
```json
{
...
  "scripts": {
    "test": "mocha ./**/*.js",
    "start": "node server.js"
  },
...
}
```
Run test:
```bash
docker-compose run --rm node npm run test
```
